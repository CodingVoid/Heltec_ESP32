#include "lora.h"

#include <esp_log.h>
#include <driver/gpio.h>
#include <freertos/FreeRTOS.h>
#include <freertos/task.h>
#include <driver/spi_master.h>
#include <driver/spi_common.h>
#include <esp_err.h>
#include <esp_log.h>
#include <stdint.h>
#include <stdio.h>
#include <time.h>
#include "pins.h"

//Heltec.begin(true /*DisplayEnable Enable*/, true /*LoRa Enable*/, true /*Serial Enable*/, false /*LoRa use PABOOST*/, 868E6 /*LoRa RF working band*/);
//if (!LoRa.begin(0x866E6, false)) {
//ESP_LOGE("lora.begin", "failed");
//}
//Heltec.LoRa.beginPacket();
//Heltec.LoRa.write(reinterpret_cast<const unsigned char *>("test"), 4);
//Heltec.LoRa.endPacket();


// registers
#define REG_FIFO 0x00
#define REG_OP_MODE 0x01
#define REG_FRF_MSB 0x06
#define REG_FRF_MID 0x07
#define REG_FRF_LSB 0x08
#define REG_PA_CONFIG 0x09
#define REG_LR_OCP 0X0b
#define REG_LNA 0x0c
#define REG_FIFO_ADDR_PTR 0x0d
#define REG_FIFO_TX_BASE_ADDR 0x0e
#define REG_FIFO_RX_BASE_ADDR 0x0f
#define REG_FIFO_RX_CURRENT_ADDR 0x10
#define REG_IRQ_FLAGS 0x12
#define REG_RX_NB_BYTES 0x13
#define REG_PKT_RSSI_VALUE 0x1a
#define REG_PKT_SNR_VALUE 0x1b
#define REG_MODEM_CONFIG_1 0x1d
#define REG_MODEM_CONFIG_2 0x1e
#define REG_PREAMBLE_MSB 0x20
#define REG_PREAMBLE_LSB 0x21
#define REG_PAYLOAD_LENGTH 0x22
#define REG_MODEM_CONFIG_3 0x26
#define REG_RSSI_WIDEBAND 0x2c
#define REG_DETECTION_OPTIMIZE 0x31
#define REG_DETECTION_THRESHOLD 0x37
#define REG_SYNC_WORD 0x39
#define REG_DIO_MAPPING_1 0x40
#define REG_VERSION 0x42
#define REG_PaDac 0x4d //add REG_PaDac

// modes
#define MODE_LONG_RANGE_MODE 0x80
#define MODE_SLEEP 0x00
#define MODE_STDBY 0x01
#define MODE_TX 0x03
#define MODE_RX_CONTINUOUS 0x05
#define MODE_RX_SINGLE 0x06

// PA config
//#define PA_BOOST                 0x80
//#define RFO                      0x70
// IRQ masks
#define IRQ_TX_DONE_MASK 0x08
#define IRQ_PAYLOAD_CRC_ERROR_MASK 0x20
#define IRQ_RX_DONE_MASK 0x40

#define MAX_PKT_LENGTH 255

uint64_t frequency = 868E6;
int implicitHeaderMode = 0;
int packetIndex = 0;
void (*onRxDone)(int) = NULL;
void (*onTxDone)() = NULL;



spi_device_handle_t spi_device_handle;
// Initialize a SPI bus for use in heltec LoRa boards
void lora_spi_init() {
    //spi_dma_t
    //spi_host_device_t
    spi_bus_config_t spi_config = {
        .miso_io_num = MISO,
        .mosi_io_num = MOSI,
        .sclk_io_num = SCK,
        .quadhd_io_num = -1,
        .quadwp_io_num = -1,
    };
    const spi_device_interface_config_t spi_device_config = {
        .address_bits = 8,
        .command_bits = 0,
        .dummy_bits = 0,
        .mode = 0,
        .clock_speed_hz = SPI_MASTER_FREQ_8M,
        .spics_io_num = SS,
        .queue_size = 4,
        .cs_ena_posttrans = 0,
        .cs_ena_pretrans = 0,
        .post_cb = NULL,
        .pre_cb = NULL,
    };

    ESP_ERROR_CHECK(spi_bus_initialize(VSPI_HOST, &spi_config, 0));
    ESP_ERROR_CHECK(spi_bus_add_device(VSPI_HOST, &spi_device_config, &spi_device_handle));
}

// Transmit data over SPI to SX1276 Transceiver
uint8_t lora_spi_transmit(uint8_t addr, uint8_t data) {
    uint8_t rx = 0;
    spi_transaction_t spi_transaction = {
        .addr = addr,
        .length = 8, // data length in bits
        .tx_buffer = &data,
        .rx_buffer = &rx,
        //.flags = SPI_TRANS_USE_TXDATA | SPI_TRANS_USE_RXDATA, // use .tx_data and .rx_data instead of .tx_buffer and .rx_buffer
    };
    //printf("SPI-transmit: addr: %x data: %x \n", (uint8_t)spi_transaction.addr, *((uint8_t *)spi_transaction.tx_buffer));
    ESP_ERROR_CHECK(spi_device_polling_transmit(spi_device_handle, &spi_transaction));
    //printf("transaction: rxlength: %d, data: %x\n", spi_transaction.rxlength, *((uint8_t *)spi_transaction.rx_buffer));
    return rx;
}

void lora_spi_free() {
    ESP_ERROR_CHECK(spi_bus_remove_device(spi_device_handle));
    ESP_ERROR_CHECK(spi_bus_free(VSPI_HOST));
}


static uint8_t lora_singleTransfer(uint8_t address, uint8_t value)
{
    return lora_spi_transmit(address, value);
}

uint8_t lora_readRegister(uint8_t address)
{
    return lora_singleTransfer(address & 0x7f, 0x00); // first bit 0 = read access
}

void lora_writeRegister(uint8_t address, uint8_t value)
{
    lora_singleTransfer(address | 0x80, value); // first bit 1 = write access
}

static void lora_explicitHeaderMode()
{
    implicitHeaderMode = 0;
    lora_writeRegister(REG_MODEM_CONFIG_1, lora_readRegister(REG_MODEM_CONFIG_1) & 0xfe);
}

static void lora_implicitHeaderMode()
{
    implicitHeaderMode = 1;
    lora_writeRegister(REG_MODEM_CONFIG_1, lora_readRegister(REG_MODEM_CONFIG_1) | 0x01);
}

void lora_setSpreadingFactor(int sf)
{
    if (sf < 6)
        sf = 6;
    else if (sf > 12)
        sf = 12;

    if (sf == 6) {
        lora_writeRegister(REG_DETECTION_OPTIMIZE, 0xc5);
        lora_writeRegister(REG_DETECTION_THRESHOLD, 0x0c);
    } else {
        lora_writeRegister(REG_DETECTION_OPTIMIZE, 0xc3);
        lora_writeRegister(REG_DETECTION_THRESHOLD, 0x0a);
    }
    lora_writeRegister(REG_MODEM_CONFIG_2, (lora_readRegister(REG_MODEM_CONFIG_2) & 0x0f) | ((sf << 4) & 0xf0));
}

void lora_end()
{
    // put in sleep mode
    lora_sleep();
    // stop SPI
    lora_spi_free();
    gpio_uninstall_isr_service();
}

int lora_beginPacket(int implicitHeader)
{
    // put in standby mode
    lora_idle();
    if (implicitHeader)
        lora_implicitHeaderMode();
    else
        lora_explicitHeaderMode();

    // reset FIFO address and paload length
    lora_writeRegister(REG_FIFO_ADDR_PTR, 0);
    lora_writeRegister(REG_PAYLOAD_LENGTH, 0);
    return 1;
}

int lora_endPacket(bool async)
{
    // put in TX mode
    lora_writeRegister(REG_OP_MODE, MODE_LONG_RANGE_MODE | MODE_TX);

    if (async) {
        // grace time is required for the radio
        vTaskDelay(pdMS_TO_TICKS(0.15)); //TODO
    } else {
        // wait for TX done
        while ((lora_readRegister(REG_IRQ_FLAGS) & IRQ_TX_DONE_MASK) == 0)
            vTaskDelay(1);

        // clear IRQ's
        lora_writeRegister(REG_IRQ_FLAGS, IRQ_TX_DONE_MASK);
    }

    return 1;
}

int lora_parsePacket(int size)
{
    int packetLength = 0;
    int irqFlags = lora_readRegister(REG_IRQ_FLAGS);

    if (size > 0) {
        lora_implicitHeaderMode();
        lora_writeRegister(REG_PAYLOAD_LENGTH, size & 0xff);
    } else {
        lora_explicitHeaderMode();
    }

    // clear IRQ's
    lora_writeRegister(REG_IRQ_FLAGS, irqFlags);

    if ((irqFlags & IRQ_RX_DONE_MASK) && (irqFlags & IRQ_PAYLOAD_CRC_ERROR_MASK) == 0) {
        // received a packet
        packetIndex = 0;
        // read packet length
        if (implicitHeaderMode) {
            packetLength = lora_readRegister(REG_PAYLOAD_LENGTH);
        } else {
            packetLength = lora_readRegister(REG_RX_NB_BYTES);
        }
        // set FIFO address to current RX address
        lora_writeRegister(REG_FIFO_ADDR_PTR, lora_readRegister(REG_FIFO_RX_CURRENT_ADDR));
        // put in standby mode
        lora_idle();
    } else if (lora_readRegister(REG_OP_MODE) != (MODE_LONG_RANGE_MODE | MODE_RX_SINGLE)) {
        // not currently in RX mode
        // reset FIFO address
        lora_writeRegister(REG_FIFO_ADDR_PTR, 0);
        // put in single RX mode
        lora_writeRegister(REG_OP_MODE, MODE_LONG_RANGE_MODE | MODE_RX_SINGLE);
    }
    return packetLength;
}

int lora_packetRssi()
{
    int8_t snr = 0;
    int8_t SnrValue = lora_readRegister(0x19);
    int16_t rssi = lora_readRegister(REG_PKT_RSSI_VALUE);

    if (SnrValue & 0x80) { // The SNR sign bit is 1
        // Invert and divide by 4
        snr = ((~SnrValue + 1) & 0xFF) >> 2;
        snr = -snr;
    } else {
        // Divide by 4
        snr = (SnrValue & 0xFF) >> 2;
    }
    if (snr < 0) {
        rssi = rssi - (frequency < 525E6 ? 164 : 157) + (rssi >> 4) + snr;
    } else {
        rssi = rssi - (frequency < 525E6 ? 164 : 157) + (rssi >> 4);
    }

    return (rssi);
}

float lora_packetSnr()
{
    return ((int8_t)lora_readRegister(REG_PKT_SNR_VALUE)) * 0.25;
}

size_t lora_write(const uint8_t *buffer, size_t size)
{
    int currentLength = lora_readRegister(REG_PAYLOAD_LENGTH);
    // check size
    if ((currentLength + size) > MAX_PKT_LENGTH)
        size = MAX_PKT_LENGTH - currentLength;

    // write data
    for (size_t i = 0; i < size; i++)
        lora_writeRegister(REG_FIFO, buffer[i]);

    // update length
    lora_writeRegister(REG_PAYLOAD_LENGTH, currentLength + size);
    return size;
}

int lora_available()
{
    return (lora_readRegister(REG_RX_NB_BYTES) - packetIndex);
}

int lora_read()
{
    if (!lora_available())
        return -1;

    packetIndex++;
    return lora_readRegister(REG_FIFO);
}

int lora_peek()
{
    if (!lora_available())
        return -1;

    // store current FIFO address
    int currentAddress = lora_readRegister(REG_FIFO_ADDR_PTR);
    // read
    uint8_t b = lora_readRegister(REG_FIFO);
    // restore FIFO address
    lora_writeRegister(REG_FIFO_ADDR_PTR, currentAddress);
    return b;
}

void lora_onDio0Rise()
{
    int irqFlags = lora_readRegister(REG_IRQ_FLAGS);
    // clear IRQ's
    lora_writeRegister(REG_IRQ_FLAGS, irqFlags);
    if (irqFlags & IRQ_RX_DONE_MASK)
    {
        if ((irqFlags & IRQ_PAYLOAD_CRC_ERROR_MASK) == 0)
        {
            // received a packet
            packetIndex = 0;
            // read packet length
            int packetLength = implicitHeaderMode ? lora_readRegister(REG_PAYLOAD_LENGTH) : lora_readRegister(REG_RX_NB_BYTES);
            // set FIFO address to current RX address
            lora_writeRegister(REG_FIFO_ADDR_PTR, lora_readRegister(REG_FIFO_RX_CURRENT_ADDR));
            if (onRxDone)
            {
                onRxDone(packetLength);
            }
            // reset FIFO address
            lora_writeRegister(REG_FIFO_ADDR_PTR, 0);
        }
    }
    else if (irqFlags & IRQ_TX_DONE_MASK)
    {
        // transmitting packet is done
        if (onTxDone) {
            onTxDone();
        }
    }
}

void lora_onTxDone(void (*callback)())
{
    onTxDone = callback;
    onRxDone = NULL;
    if (callback)
    {
        lora_writeRegister(REG_DIO_MAPPING_1, 0x40);
        //hook isr handler for specific gpio pin
        gpio_isr_handler_add(LORA_DEFAULT_DIO0_PIN, lora_onDio0Rise, (void *)NULL);
    }
    else
    {
        //remove isr handler for gpio number.
        gpio_isr_handler_remove(LORA_DEFAULT_DIO0_PIN);
    }
}

void lora_onRxDone(void (*callback)(int))
{
    onRxDone = callback;
    onTxDone = NULL;
    if (callback)
    {
        lora_writeRegister(REG_DIO_MAPPING_1, 0x00);
        //hook isr handler for specific gpio pin
        gpio_isr_handler_add(LORA_DEFAULT_DIO0_PIN, lora_onDio0Rise, (void *)NULL);
    }
    else
    {
        //remove isr handler for gpio number.
        gpio_isr_handler_remove(LORA_DEFAULT_DIO0_PIN);
    }
}

void lora_receive(int size)
{
    if (size > 0)
    {
        lora_implicitHeaderMode();
        lora_writeRegister(REG_PAYLOAD_LENGTH, size & 0xff);
    }
    else
    {
        lora_explicitHeaderMode();
    }

    lora_writeRegister(REG_OP_MODE, MODE_LONG_RANGE_MODE | MODE_RX_CONTINUOUS);
}

void lora_idle()
{
    lora_writeRegister(REG_OP_MODE, MODE_LONG_RANGE_MODE | MODE_STDBY);
}

void lora_sleep()
{
    lora_writeRegister(REG_OP_MODE, MODE_LONG_RANGE_MODE | MODE_SLEEP);
}

void lora_setTxPower(int8_t power, int8_t outputPin)
{
    uint8_t paConfig = 0;
    uint8_t paDac = 0;

    paConfig = lora_readRegister(REG_PA_CONFIG);
    paDac = lora_readRegister(REG_PaDac);

    paConfig = (paConfig & RF_PACONFIG_PASELECT_MASK) | outputPin;
    paConfig = (paConfig & RF_PACONFIG_MAX_POWER_MASK) | 0x70;

    if ((paConfig & RF_PACONFIG_PASELECT_PABOOST) == RF_PACONFIG_PASELECT_PABOOST) {
        if (power > 17) {
            paDac = (paDac & RF_PADAC_20DBM_MASK) | RF_PADAC_20DBM_ON;
        } else {
            paDac = (paDac & RF_PADAC_20DBM_MASK) | RF_PADAC_20DBM_OFF;
        }
        if ((paDac & RF_PADAC_20DBM_ON) == RF_PADAC_20DBM_ON) {
            if (power < 5) {
                power = 5;
            }
            if (power > 20) {
                power = 20;
            }
            paConfig = (paConfig & RF_PACONFIG_OUTPUTPOWER_MASK) | (uint8_t)((uint16_t)(power - 5) & 0x0F);
        } else {
            if (power < 2) {
                power = 2;
            }
            if (power > 17) {
                power = 17;
            }
            paConfig = (paConfig & RF_PACONFIG_OUTPUTPOWER_MASK) | (uint8_t)((uint16_t)(power - 2) & 0x0F);
        }
    } else {
        if (power < -1) {
            power = -1;
        }
        if (power > 14) {
            power = 14;
        }
        paConfig = (paConfig & RF_PACONFIG_OUTPUTPOWER_MASK) | (uint8_t)((uint16_t)(power + 1) & 0x0F);
    }
    lora_writeRegister(REG_PA_CONFIG, paConfig);
    lora_writeRegister(REG_PaDac, paDac);
}

void lora_setTxPowerMax(int level)
{
    if (level < 5)
        level = 5;
    else if (level > 20)
        level = 20;

    lora_writeRegister(REG_LR_OCP, 0x3f);
    lora_writeRegister(REG_PaDac, 0x87); //Open PA_BOOST
    lora_writeRegister(REG_PA_CONFIG, RF_PACONFIG_PASELECT_PABOOST | (level - 5));
}

void lora_setFrequency(long freq)
{
    frequency = freq;
    uint64_t frf = ((uint64_t)frequency << 19) / 32000000;
    lora_writeRegister(REG_FRF_MSB, (uint8_t)(frf >> 16));
    lora_writeRegister(REG_FRF_MID, (uint8_t)(frf >> 8));
    lora_writeRegister(REG_FRF_LSB, (uint8_t)(frf >> 0));
}

void lora_setSignalBandwidth(long sbw)
{
    int bw;

    if (sbw <= 7.8E3)
        bw = 0;
    else if (sbw <= 10.4E3)
        bw = 1;
    else if (sbw <= 15.6E3)
        bw = 2;
    else if (sbw <= 20.8E3)
        bw = 3;
    else if (sbw <= 31.25E3)
        bw = 4;
    else if (sbw <= 41.7E3)
        bw = 5;
    else if (sbw <= 62.5E3)
        bw = 6;
    else if (sbw <= 125E3)
        bw = 7;
    else if (sbw <= 250E3)
        bw = 8;
    else /*if (sbw <= 250E3)*/
        bw = 9;
    lora_writeRegister(REG_MODEM_CONFIG_1, (lora_readRegister(REG_MODEM_CONFIG_1) & 0x0f) | (bw << 4));
}

void lora_setCodingRate4(int denominator)
{
    if (denominator < 5)
        denominator = 5;
    else if (denominator > 8)
        denominator = 8;

    int cr = denominator - 4;
    lora_writeRegister(REG_MODEM_CONFIG_1, (lora_readRegister(REG_MODEM_CONFIG_1) & 0xf1) | (cr << 1));
}

void lora_setPreambleLength(long length)
{
    lora_writeRegister(REG_PREAMBLE_MSB, (uint8_t)(length >> 8));
    lora_writeRegister(REG_PREAMBLE_LSB, (uint8_t)(length >> 0));
}

void lora_setSyncWord(int sw)
{
    lora_writeRegister(REG_SYNC_WORD, sw);
}

void lora_enableCrc()
{
    lora_writeRegister(REG_MODEM_CONFIG_2, lora_readRegister(REG_MODEM_CONFIG_2) | 0x04);
}

void lora_disableCrc()
{
    lora_writeRegister(REG_MODEM_CONFIG_2, lora_readRegister(REG_MODEM_CONFIG_2) & 0xfb);
}

char lora_random()
{
    return lora_readRegister(REG_RSSI_WIDEBAND);
}

void lora_dumpRegisters(FILE *out)
{
    for (int i = 0; i < 128; i++)
        fprintf(out, "0x%x :0x%x\n", i, lora_readRegister(i));
}

int lora_begin(bool PABOOST)
{
    // setup pins
    gpio_config_t gpioConfigOut = {
        .pull_up_en = 0,                                                                  // disable pull-up mode
        .pull_down_en = 0,                                                                // disable pull-down mode
        .pin_bit_mask = (1ULL << LORA_DEFAULT_SS_PIN) | (1ULL << LORA_DEFAULT_RESET_PIN), // configure Slave-Select and Reset pins
        .intr_type = GPIO_INTR_DISABLE,                                                   // disable interrupts
        .mode = GPIO_MODE_OUTPUT,                                                         // set Slave-Select and Reset Pins as output
    };
    gpio_config_t gpioConfigIn = {
        .pull_up_en = 1,                                 // enable pull-up mode
        .pin_bit_mask = (1ULL << LORA_DEFAULT_DIO0_PIN), // configure dio0 pin
        .intr_type = GPIO_INTR_POSEDGE,                  // Interrupt on Rising/Positive Edge
        .mode = GPIO_MODE_INPUT,                         // set dio0 as input pin
    };
    ESP_ERROR_CHECK(gpio_config(&gpioConfigOut));
    ESP_ERROR_CHECK(gpio_config(&gpioConfigIn));
    // perform reset
    ESP_ERROR_CHECK(gpio_set_level(LORA_DEFAULT_RESET_PIN, 0));
    vTaskDelay(pdMS_TO_TICKS(20));
    ESP_ERROR_CHECK(gpio_set_level(LORA_DEFAULT_RESET_PIN, 1));
    vTaskDelay(pdMS_TO_TICKS(50));
    // set SS high
    ESP_ERROR_CHECK(gpio_set_level(LORA_DEFAULT_SS_PIN, 1));
    lora_spi_init();
    // Install the driver’s GPIO ISR handler service, which allows per-pin GPIO interrupt handlers. (for DIO0 Interrupt on RX Receive)
    ESP_ERROR_CHECK(gpio_install_isr_service(0)); // no flags
    // check version
    uint8_t version = lora_readRegister(REG_VERSION);
    if (version != 0x12)
    {
        printf("Version not correct\n");
        return 0;
    }

    // put in sleep mode
    lora_sleep();
    // set frequency
    lora_setFrequency(frequency);
    // set base addresses
    lora_writeRegister(REG_FIFO_TX_BASE_ADDR, 0);
    lora_writeRegister(REG_FIFO_RX_BASE_ADDR, 0);
    // set LNA boost
    lora_writeRegister(REG_LNA, lora_readRegister(REG_LNA) | 0x03);
    // set auto AGC
    lora_writeRegister(REG_MODEM_CONFIG_3, 0x04);
    // set output power to 14 dBm
    if (PABOOST == true)
        lora_setTxPower(14, RF_PACONFIG_PASELECT_PABOOST);
    else
        lora_setTxPower(14, RF_PACONFIG_PASELECT_RFO);
    // Set default Spreading Factor to 11. Low Energy Consumption + High Bandwith, but lower range
    lora_setSpreadingFactor(11);
    // set Signal Bandwidth to 125kHz, allowed are 125kHz, 250kHz, 500kHz
    lora_setSignalBandwidth(125E3);
    //setCodingRate4(5);
    // value 0x34 is reserved for LoRaWAN Networks, so we use it as default
    lora_setSyncWord(0x34); 
    lora_disableCrc();
    lora_enableCrc();
    // put in standby mode
    lora_idle();

    return 1;
}
