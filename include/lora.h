#ifndef LORA_H
#define LORA_H

#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>

#define LORA_DEFAULT_SS_PIN 18
#define LORA_DEFAULT_RESET_PIN 14
#define LORA_DEFAULT_DIO0_PIN 26

#define PA_OUTPUT_PA_BOOST_PIN 1
#define PA_OUTPUT_RFO_PIN 0

/*!
 * RegPaConfig
 */
#define RF_PACONFIG_PASELECT_MASK 0x7F
#define RF_PACONFIG_PASELECT_PABOOST 0x80
#define RF_PACONFIG_PASELECT_RFO 0x00 // Default

#define RF_PACONFIG_MAX_POWER_MASK 0x8F

#define RF_PACONFIG_OUTPUTPOWER_MASK 0xF0

/*!
 * RegPaDac
 */
#define RF_PADAC_20DBM_MASK 0xF8
#define RF_PADAC_20DBM_ON 0x07
#define RF_PADAC_20DBM_OFF 0x04 // Default

#if defined(__STM32F1__)
inline unsigned char digitalPinToInterrupt(unsigned char Interrupt_pin) { return Interrupt_pin; } //This isn't included in the stm32duino libs (yet)
#define portOutputRegister(port) (volatile byte *)(&(port->regs->ODR))                            //These are defined in STM32F1/variants/generic_stm32f103c/variant.h but return a non byte* value
#define portInputRegister(port) (volatile byte *)(&(port->regs->IDR))                             //These are defined in STM32F1/variants/generic_stm32f103c/variant.h but return a non byte* value
#endif

// SPI stuff
void lora_spi_init();
void lora_spi_free();
uint8_t lora_spi_transmit(uint8_t addr, uint8_t data);

// public
int lora_begin(bool PABOOST);
void lora_end();

int lora_beginPacket(int implicitHeader); // default: false
int lora_endPacket(bool async); // default: false

int lora_parsePacket(int size); // default: 0
int lora_packetRssi();
float lora_packetSnr();

// from Print
size_t lora_write(const uint8_t *buffer, size_t size);

// from Stream
int lora_available();
int lora_read();
int lora_peek();
void lora_flush();

// callback can either be used to signal TxDone (Transmission done) or RxDone (onReceive) but not both. if either one of the callbacks is set, the other will be overwritten
void lora_onTxDone(void (*callback)());
void lora_onRxDone(void (*callback)(int));

void lora_receive(int size); // default: 0
void lora_idle();
void lora_sleep();

void lora_setTxPower(int8_t power, int8_t outputPin);
void lora_setTxPowerMax(int level);
void lora_setFrequency(long frequency);
void lora_setSpreadingFactor(int sf);
void lora_setSignalBandwidth(long sbw);
void lora_setCodingRate4(int denominator);
void lora_setPreambleLength(long length);
void lora_setSyncWord(int sw);
void lora_enableCrc();
void lora_disableCrc();

void lora_enableCrc();
void lora_disableCrc();

char lora_random();

void lora_setPins(int ss, int reset, int dio0);
void lora_setSPIFrequency(uint32_t frequency);

void lora_dumpRegisters(FILE *out);

#endif
