#include <sdkconfig.h>

#if defined(CONFIG_HELTEC_WIFI_KIT_32)
#include "variants/heltec_wifi_kit_32/pins.h"
#elif defined(CONFIG_HELTEC_WIFI_LORA_32)
#include "variants/heltec_wifi_lora_32/pins.h"
#elif defined(CONFIG_HELTEC_WIFI_LORA_32_V2)
#include "variants/heltec_wifi_lora_32_V2/pins.h"
#elif defined(CONFIG_HELTEC_WIRELESS_STICK)
#include "variants/heltec_wireless_stick/pins.h"
#elif defined(CONFIG_HELTEC_WIRELESS_STICK_LITE)
#include "variants/heltec_wireless_stick_lite/pins.h"
#else
#error One of the possible boards must be defined.
#endif